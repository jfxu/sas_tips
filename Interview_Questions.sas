/* 1. What kind of procedure have you used? */
/* 2. Have you used SQL? then how to create a data set? merge? join? different kinds of join? */
/*    what is the difference between where and having? */
/* 3. How to find the total observation numbers? */
/* 4. Have you developed any macros? array? */
/* 5. Have you used call execute? */
/* 6. regular expression in SAS? */
/* 7. What kind of SAS textbook have you read? */
/* 8. How to find solutions when */
/* 9. Could you show me some when you use data null */


/*********************************************************/
/* Tricky Questions? */
/* 1. How to use data step to transform */

data transposed;
   set base;
   array Qtr{3} Q:;
   do i = 1 to 3;
      Period = cat('Qtr',i);
      Amount = Qtr{i} ;
      output;
   end;
   drop Q1:Q3;
   if Amount ne .;
run;

proc transpose data = base out = transposed
   (rename=(Col1=Amount) where=(Amount ne .)) name=Period;
   by cust;
run;

/* 2. difference between input and infile */


### R questions
1. What kinds of R packages have used?
2. How to find missing values in a vector, matrix, data.frame?
3. how to generate a data.frame?
4. Have you used apply functions? like apply, sapply, lapply? what is the difference between sapply and lapply?